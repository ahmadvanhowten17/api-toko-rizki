<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductCategori;
class ProductController extends Controller
{
    public function all(Request $request)
    {
        $id = $request->input('id');
        $limit = $request->input('limit',6);
        $name = $request->input('name');
        $slug = $request->input('slug');
        $price_from = $request->input('price_from');
        $price_to = $request->input('price_to');

       if ($id) {
           $product = Product::with('category','galeries')->find($id);
           if ($product) {
                return ResponseFormated::success($product,'data berhasil di tambahkan');
           }else {
               return ResponseFormated::error($product,'data gagal di tambahkan',404);
           }
       }
       if ($slug) {
           $product = Product::with('category','galeries')->where('slug',$slug)->first();
           if ($product) {
                return ResponseFormated::success($product,'data berhasil di tambahkan');
           }else {
               return ResponseFormated::error($product,'data gagal di tambahkan',404);
           }
       }
       $product = Product::with('category','galeries');
       if ($name) {
           $product->where('name','like','%'.$name.'%');
       }
       if ($price_from) {
           $product->where('price', '>=', $price_from);
       }
       if ($price_to) {
        $product->where('price', '<=', $price_to);
       }
       return ResponseFormated::success(
           $product->paginate($limit),'data berhasil di tambahkan'
       );
    }
}
