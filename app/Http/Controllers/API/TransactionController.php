<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
class TransactionController extends Controller
{
    public function detail(Request $request, $id)
    {
        $product = Transaction::with('details.product')->find($id);

        if ($product) {
            return ResponseFormated::success($product,'Data berhasil di ambil');
        }else {
            return ResponseFormated::error(null,'Data Gagal  di ambil',404);
        }
    }
}
