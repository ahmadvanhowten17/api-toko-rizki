<?php

namespace App\Http\Controllers;

use App\Models\ProductCategori;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductCategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $record = ProductCategori::get();
        return view('pages.categori.index',[
            'record' => $record
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.categori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['nama' => 'required|max:15']);
        $data = $request->all();
        $data['slug'] = str::slug($request->nama);
        $kategori = ProductCategori::create($data);
        return redirect('categori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.categori.edit',[
            'record' => ProductCategori::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(['nama' => 'required|max:15']);
        $data = $request->all();
        $data['slug'] = str::slug($request->nama);
        $kategori = ProductCategori::findOrFail($id);
        $kategori->update($data);
        return redirect('categori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = ProductCategori::findOrFail($id);
        $kategori->delete();
        return redirect('categori');
    }
}
