<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function galeries()
    {
        return $this->hasMany(ProductGalerie::class);
    }
    public function category()
    {
        return $this->belongsTo(ProductCategori::class, 'category_id', 'id');
    }
}
