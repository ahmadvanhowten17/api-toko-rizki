<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
class ProductCategori extends Model
{
    protected $guarded = [];
    protected $table = 'categories';

    public function categories()
    {
        return $this->hasMany(Product::class,'category_id');
    }
}
