<?php

namespace App\Models;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductGalerie extends Model
{
    use SoftDeletes;
    
    protected $table = 'product_galeries';
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(Product::class,'products_id','id');
    }
    public function getPhotoAttribute($value)
    {
        return url('storage/' .$value);
    }
}
