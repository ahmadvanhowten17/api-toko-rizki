<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     // return view('welcome');
// });

Auth::routes();
Route::get('/','AdminController@index');
Route::resource('/product', 'ProductController');
Route::resource('/categori', 'ProductCategoriController');
Route::resource('/transaction', 'TransactionController');
Route::get('transaction/{id}/set-status','TransactionController@setstatus')->name('status');
Route::get('/home', 'HomeController@index')->name('home');
