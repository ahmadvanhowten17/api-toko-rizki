@extends('layouts.default')
@section('content')
    <div class="orders">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Daftar Transaksi</h4>
                    </div>
                    <div class="card-body--">
                        <div class="table-stats order-table ov-h">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>uuid</th>
                                        <th>Nama</th>
                                        <th>Number</th>
                                        <th>address</th>
                                        <th>Transaksi total</th>
                                        <th>Status</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; ?>
                                    @foreach ($record as $item)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $item->uuid }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->number }}</td>
                                            <td>{{ $item->address }}</td>
                                            <td>{{ 'Rp '. number_format($item->transaction_total,2,',','.') }}</td>
                                            <td>
                                                @switch($item->transaction_status)
                                                    @case('PENDING')
                                                        <span class="badge badge-primary">PENDING</span>
                                                        @break
                                                    @case('SUCCESS')
                                                        <span class="badge badge-success">SUCCESS</span>
                                                        @break
                                                    @case('FAILED')
                                                        <span class="badge badge-warning">FAILED</span>
                                                        @break
                                                    @default

                                                @endswitch
                                            </td>
                                            <td>
                                                @if ($item->transaction_status == 'PENDING')
                                                <a href="{{ route('status',$item->id) }}?status=SUCCESS" class="btn btn-success btn-sm">
                                                <i class="fa fa-check"></i>
                                                </a>
                                                <a href="{{ route('status',$item->id) }}?status=FAILED" class="btn btn-warning btn-sm">
                                                <i class="fa fa-times"></i>
                                                </a>
                                                @endif
                                                <a href="#mymodal" data-remote="{{ url('transaction',$item->id) }}" data-target="#mymodal" data-toggle="modal" data-title="Detail Transaksi {{ $item->uuid }}" class="btn btn-sm btn-info">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a href="{{ url("transaction/$item->id/edit") }}" class="btn btn-sm btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <form action="{{ route('transaction.destroy',$item->id) }}" method="post" class="d-inline">
                                                    @csrf
                                                    @method('delete')
                                                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php $no++ ?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="mymodal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                      </button> --}}
                    <h5 class="modal-title"></h5>
                </div>
                <div class="modal-body">
                    <i class="fa fa-spinner fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
@endsection
