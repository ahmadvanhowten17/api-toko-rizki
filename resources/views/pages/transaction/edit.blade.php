@extends('layouts.default')
@section('content')
    <div class="card">
        <h4 class="card-header">Update Transaksi</h4>
        <div class="card-body">
            <form action="{{ url('transaction',$record->id) }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="">Nama</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Masukan Nmaa.." value="{{ old('name') ?? $record->name }}">
                    @error('name')
                        <div class="text-muted">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="email" placeholder="Masukan Nmaa.." value="{{ old('email') ?? $record->email }}">
                    @error('email')
                        <div class="text-muted">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Number</label>
                    <input type="text" class="form-control @error('number') is-invalid @enderror" name="number" placeholder="Masukan Nmaa.." value="{{ old('number') ?? $record->number }}">
                    @error('number')
                        <div class="text-muted">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Address</label>
                    <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" placeholder="Masukan Nmaa.." value="{{ old('address') ?? $record->address }}">
                    @error('address')
                        <div class="text-muted">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
            </form>
        </div>
    </div>
@endsection
