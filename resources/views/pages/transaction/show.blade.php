<table class="table table-bordered">
    <tr>
        <th>Nama</th>
        <td>{{ $record->name }}</td>
    </tr>
    <tr>
        <th>Email</th>
        <td>{{ $record->email }}</td>
    </tr>
    <tr>
        <th>Number</th>
        <td>{{ $record->number }}</td>
    </tr>
    <tr>
        <th>Address</th>
        <td>{{ $record->address }}</td>
    </tr>
    <tr>
        <th>Transaksi</th>
        <td>{{ $record->transaction_total }}</td>
    </tr>
    <tr>
        <th>Status</th>
        <td>{{ $record->transaction_status }}</td>
    </tr>
    <tr>
        <th>Pembelian Product</th>
        <td>
            <table class="table table-bordered w-100">
                <tr>
                    <th>Nama</th>
                    <th>Kategori</th>
                    <th>Harga</th>
                </tr>
                @foreach ($record->details as $detail)
                @php
                    $product = App\Models\ProductCategori::where('id',$detail->product->category_id)->first();
                @endphp
                <tr>
                    <td>{{ $detail->product->name }}</td>
                    <td>{{ $product->nama }}</td>
                    <td>{{ $detail->product->price }}</td>
                </tr>
                @endforeach
            </table>
        </td>
    </tr>
</table>
<div class="row">
    <div class="col-4">
        <a href="{{ route('status',$record->id) }}?status=SUCCESS" class="btn btn-success btn-sm btn-block">
            <i class="fa fa-check"></i>Set Success
        </a>
    </div>
    <div class="col-4">
        <a href="{{ route('status',$record->id) }}?status=FAILED" class="btn btn-warning btn-sm btn-block">
            <i class="fa fa-times"></i>Set Gagal
        </a>
    </div>
    <div class="col-4">
        <a href="{{ route('status',$record->id) }}?status=PENDING" class="btn btn-primary btn-sm btn-block">
            <i class="fa fa-spinner"></i>Set Pending
        </a>
    </div>
</div>
