@extends('layouts.default')
@section('content')
    <div class="card">
        <h4 class="card-header">Tambah Product</h4>
        <div class="card-body">
            <form action="{{ url('product',$record->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <input type="hidden" name="id" value="{{ $record->id }}">
                <div class="form-group">
                    <label for="">Nama Barang</label>
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Nama Barang" value="{{ $record->name }}">
                    @error('name')
                        <div class="text-muted">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Kategori</label>
                    <select name="category_id" id="" class="form-control">
                        <option value="">Pilih Kategori</option>
                        @foreach ($kategori as $item)
                            <option value="{{ $item->id }}" {{ $record->category_id == $item->id ? 'selected' : '' }}>{{ $item->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Description</label>
                    <textarea name="description" id="editor" cols="30" rows="10" class="form-control @error('description') is-invalid @enderror">{{ $record->description }}</textarea>
                    @error('description')
                        <div class="text-muted">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Harga Barang</label>
                    <input type="text" name="price" class="form-control @error('price') is-invalid @enderror" placeholder="Harga Barang" value="{{ $record->price }}">
                    @error('price')
                        <div class="text-muted">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Quantity Barang</label>
                    <input type="text" name="quantity" class="form-control @error('quantity') is-invalid @enderror" placeholder="Harga Barang" value="{{ $record->quantity }}">
                    @error('quantity')
                        <div class="text-muted">{{ $message }}</div>
                    @enderror
                </div>
                <div class="card-body">
                    <div class="row">
                        @if ($record->galeries->count() > 0)
                            @foreach ($record->galeries as $gam)
                                <div class="col-lg-4">
                                    <div class="text-center">
                                        <img src="{{ $gam->photo }}" alt="" height="150">
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Picture</label>
                    <input type="file" name="photo" id="" class="form-control @error('photo') is-invalid @enderror" accept="image/*">
                    @error('photo')
                        <div class="text-muted">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
@endsection
@push('ckeditor')
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
    <script type="text/javascript">
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .then( editor => {
                    console.log( editor );
            } )
            .catch( error => {
                    console.error( error );
            });
    </script>
@endpush
