@extends('layouts.default')
@section('content')
    <div class="orders">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Daftar Product</h4>
                    </div>
                    <div class="card-body--">
                        <div class="table-stats order-table ov-h">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Kategori</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Photo</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; ?>
                                    @foreach ($record as $item)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->category->nama }}</td>
                                            <td>{{ $item->price }}</td>
                                            <td>{{ $item->quantity }}</td>
                                            @if ($item->galeries->count() > 0)
                                                <td><img src="{{ $item->galeries->first()->photo }}" alt=""></td>
                                            @endif
                                            {{-- @foreach ($item->galeries as $items)
                                                <td><img src="{{ $items->photo ?? '' }}" alt=""></td>
                                            @endforeach --}}
                                            <td>
                                                <a href="" class="btn btn-sm btn-info">
                                                    <i class="fa fa-picture-o"></i>
                                                </a>
                                                <a href="{{ url("product/$item->id/edit") }}" class="btn btn-sm btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <form action="{{ route('product.destroy',$item->id) }}" method="post" class="d-inline">
                                                    @csrf
                                                    @method('delete')
                                                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php $no++ ?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
