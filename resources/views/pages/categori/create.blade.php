@extends('layouts.default')
@section('content')
    <div class="card">
        <h4 class="card-header">Tambah Categori</h4>
        <div class="card-body">
            <form action="{{ url('categori') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="">Nama</label>
                    <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" placeholder="Nama Barang" value="{{ old('nama') }}">
                    @error('nama')
                        <div class="text-muted">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
            </form>
        </div>
    </div>
@endsection

