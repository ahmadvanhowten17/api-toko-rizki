@extends('layouts.default')
@section('content')
    <div class="orders">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Daftar Product</h4>
                    </div>
                    <div class="card-body--">
                        <div class="table-stats order-table ov-h">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Slug</th>
                                        <th>Nama</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($record->count() > 0)
                                        <?php $no = 1; ?>
                                        @foreach ($record as $item)
                                            <tr>
                                                <td>{{ $no }}</td>
                                                <td>{{ $item->slug }}</td>
                                                <td>{{ $item->nama }}</td>
                                                <td>
                                                    <a href="{{ url("categori/$item->id/edit") }}" class="btn btn-sm btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <form action="{{ route('categori.destroy',$item->id) }}" method="post" class="d-inline">
                                                        @csrf
                                                        @method('delete')
                                                        <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            <?php $no++ ?>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td>Data Masih Kosong</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
