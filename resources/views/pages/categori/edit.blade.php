@extends('layouts.default')
@section('content')
    <div class="card">
        <h4 class="card-header">Tambah Categori</h4>
        <div class="card-body">
            <form action="{{ url('categori',$record->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="">Nama</label>
                    <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" placeholder="Nama Barang" value="{{ $record->nama }}">
                    @error('nama')
                        <div class="text-muted">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
            </form>
        </div>
    </div>
@endsection
